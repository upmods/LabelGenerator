<?php

require "Modules/LabelGenerator.php";

$labelDescr1 = array
(
		"TopMargin" => 0.5,
		"BottomMargin" => 0.5,
		"LeftMargin" => 0.1875,
		"RightMargin" => 0.1875,

		"HorizSpacing" => 0.125,
		"VertSpacing" => 0,

		"LabelSize" => array
		(
		  "Width"=>2.625,
		  "Height"=>1,
		),

		"PaperSize" => array
		(
		  "Width"=> 8.5,
		  "Height"=> 11,
		),

		"LabelTableSize" => array
		(
		  "Width"=>3,
		  "Height"=>10,
		),
);


header("Content-type:  image/gif");

$image = new LabelGenerator();
$image->render($labelDescr1);
