<?php
/*
 * LIBRARY GENERATE GRID LABERS 
*/

class LabelGenerator {
    
    public $pixel;
    public $width;
    public $height;
    public $topMargin;
    public $bottomMargin;
    public $leftMargin;
    public $rightMargin;    
    public $labelHeight;
    public $labelWidth;     
    public $labelHorizSpacing;
    public $labelVertSpacing;
    public $labelCountHoriz;
    public $labelCountVert;

    public function __construct()
    {
        // SET DEFAULT UNIT AS INCH. ONE INCH HAVE 96 PIXELS
        $this->pixel = 96;
    }
    
    /*
    *  RENDER IMAGE FUNCTION
    */
    public function render($params)
    {
        // SET PARAMETERS
        $this->height = (!empty($params['PaperSize']['Height'])) ? $params['PaperSize']['Height'] * $this->pixel : 0;  
        $this->width  = (!empty($params['PaperSize']['Width'])) ? $params['PaperSize']['Width'] * $this->pixel : 0;
        $this->topMargin = (!empty($params['TopMargin'])) ? $params['TopMargin'] * $this->pixel : 0;  
        $this->bottomMargin  = (!empty($params['BottomMargin'])) ? $params['BottomMargin'] * $this->pixel : 0;
        $this->leftMargin = (!empty($params['LeftMargin'])) ? $params['LeftMargin'] * $this->pixel : 0;  
        $this->rightMargin  = (!empty($params['RightMargin'])) ? $params['RightMargin'] * $this->pixel : 0;         
        $this->labelHeight = (!empty($params['LabelSize']['Height'])) ? $params['LabelSize']['Height'] * $this->pixel : 0;  
        $this->labelWidth  = (!empty($params['LabelSize']['Width'])) ? $params['LabelSize']['Width'] * $this->pixel : 0;
        $this->labelCountHoriz = (!empty($params['LabelTableSize']['Width'])) ? $params['LabelTableSize']['Width'] : 0;  
        $this->labelCountVert  = (!empty($params['LabelTableSize']['Height'])) ? $params['LabelTableSize']['Height'] : 0;                
        $this->labelHorizSpacing = (!empty($params['HorizSpacing'])) ? $params['HorizSpacing'] * $this->pixel : 0;  
        $this->labelVertSpacing  = (!empty($params['VertSpacing'])) ? $params['VertSpacing'] * $this->pixel : 0;          
        
        // CREATE IMAGES OBJECTS
        $image = imageCreate($this->width, $this->height); 
        
        // SET COLORS
        $colorBackgr = imageColorAllocate($image, 192, 192, 192);
        $colorGrid   = imageColorAllocate($image, 0, 0, 0);
        $colorLine   = imageColorAllocate($image, 148, 152, 161);  

        // SET TRANSPARENT BACKGROUND COLOR
        imageColorTransparent($image, $colorBackgr);
        
        // SET BORDER WIDTH
        imagesetthickness($image, 2);
        
        //SET START PARAMS BEFORE DRAW 
        $topMargin = $this->topMargin;
        $leftMargin = $this->leftMargin;
        $labelWidth = $this->labelWidth;
        $labelHeight = $this->labelHeight;
        
        // START DRAWING
        for($i = 1; $i <= $this->labelCountVert; $i++){
            $labelHeight = $topMargin + $this->labelHeight;
            $leftMargin = $this->leftMargin;
            for($j = 1; $j <= $this->labelCountHoriz; $j++){
                $labelWidth = (int)$this->labelWidth * $j;
                imageRectangle($image, $leftMargin, $topMargin , $labelWidth, $labelHeight, $colorGrid);
                $leftMargin = ($this->labelWidth * $j) + $this->labelHorizSpacing;
            }
            $topMargin += $this->labelHeight + $this->labelVertSpacing;
        }        

        // RENDER IMAGE
        imageGIF($image);
    }
}
