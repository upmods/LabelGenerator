<?php

require "Modules/LabelGenerator.php";


$labelDescr2 = array
(
		"TopMargin" => 25.4,
		"BottomMargin" => 25.4,
		"LeftMargin" => 31.75,
		"RightMargin" => 31.75,

		"HorizSpacing" => 0,
		"VertSpacing" => 25.4,

		"LabelSize" => array
		(
		  "Width"=>152.4,
		  "Height"=>101.6,
		),

		"PaperSize" => array
		(
		  "Width"=>215.9,
		  "Height"=>279.4,
		),

		"LabelTableSize" => array
		(
		  "Width"=>1,
		  "Height"=>2,
		),
);

header("Content-type:  image/gif");

$image = new LabelGenerator();
$image->render($labelDescr2);
